import { initialise } from '../../lambda/faas';

describe('Lambda functions', () => {
  const mockApiGatewayEvent = {
      body: undefined,
      headers: undefined,
      httpMethod: 'GET',
      isBase64Encoded: false,
      path: '/arbitrary/arbitrary-route/',
      pathParameters: {
        tail: '/arbitrary-route/',
      },
      queryStringParameters: undefined,
      requestContext: undefined,
      resource: 'arbitrary',
      stageVariables: undefined,
    };

  it('should not call the callback for GET methods when no GET-middleware is configured', (done) => {
    const callback = jest.fn();

    const faas = initialise({
      ...mockApiGatewayEvent,
      httpMethod: 'GET',
    }, callback);
    faas.post('/arbitrary-route', () => Promise.resolve({ arbitrary: 'response' }));

    setImmediate(() => {
      expect(callback.mock.calls.length).toBe(0);

      done();
    });
  });

  it('should not call the callback for POST methods when no POST-middleware is configured', (done) => {
    const callback = jest.fn();

    const faas = initialise({
      ...mockApiGatewayEvent,
      httpMethod: 'POST',
    }, callback);
    faas.put('/arbitrary-route', () => Promise.resolve({ arbitrary: 'response' }));

    setImmediate(() => {
      expect(callback.mock.calls.length).toBe(0);

      done();
    });
  });

  it('should respond with all supported HTTP verbs and headers on OPTIONS requests', (done) => {
    const callback = jest.fn();

    const faas = initialise({
      ...mockApiGatewayEvent,
      httpMethod: 'OPTIONS',
    }, callback);
    faas.put('/arbitrary-route', () => Promise.resolve({ arbitrary: 'response' }));
    faas.default(() => Promise.resolve({ arbitrary: 'response' }));

    setImmediate(() => {
      expect(callback.mock.calls.length).toBe(1);
      expect(callback.mock.calls[0][1].headers).toEqual({
        'Access-Control-Allow-Headers': 'Authorization',
        'Access-Control-Allow-Methods': 'PUT',
      });

      done();
    });
  });

  it('should log an error and skip the middleware if no callback is defined for OPTIONS requests', () => {
    console.log = jest.fn();

    const faas = initialise({
      ...mockApiGatewayEvent,
      httpMethod: 'OPTIONS',
    });

    const mockMiddleware = jest.fn();

    faas.default(mockMiddleware);

    expect(mockMiddleware.mock.calls.length).toBe(0);
    expect(console.log.mock.calls.length).toBe(1);
    expect(console.log.mock.calls[0][0]).toBe('No response callback defined by API Gateway.');
  });

  it('should not call the callback for PUT methods when no PUT-middleware is configured', (done) => {
    const callback = jest.fn();

    const faas = initialise({
      ...mockApiGatewayEvent,
      httpMethod: 'PUT',
    }, callback);
    faas.get('/arbitrary-route', () => Promise.resolve({ arbitrary: 'response' }));

    setImmediate(() => {
      expect(callback.mock.calls.length).toBe(0);

      done();
    });
  });

  it('should not call the callback for invalid methods if no default response is defined', (done) => {
    const callback = jest.fn();

    const faas = initialise({
      ...mockApiGatewayEvent,
      httpMethod: 'INVALID_METHOD',
    }, callback);
    faas.get('/arbitrary-route', () => Promise.resolve({ arbitrary: 'response' }));

    setImmediate(() => {
      expect(callback.mock.calls.length).toBe(0);

      done();
    });
  });

  it('should log an error and skip the middleware if no callback is defined', () => {
    console.log = jest.fn();

    const faas = initialise({
      ...mockApiGatewayEvent,
      httpMethod: 'GET',
      path: '/arbitrary/some-route/',
      pathParameters: {
        tail: 'some-route/',
      },
    });

    const mockMiddleware = jest.fn();

    faas.get('/some-route', mockMiddleware);

    expect(mockMiddleware.mock.calls.length).toBe(0);
    expect(console.log.mock.calls.length).toBe(1);
    expect(console.log.mock.calls[0][0]).toBe('No response callback defined by API Gateway.');
  });

  it('should return the default response if no middleware matches the route', (done) => {
    const callback = jest.fn();

    const faas = initialise(mockApiGatewayEvent, callback);
    faas.default(() => Promise.resolve({ some: 'response' }));

    setImmediate(() => {
      expect(callback.mock.calls.length).toBe(1);
      expect(callback.mock.calls[0][1]).toEqual({
        body: '{"some":"response"}',
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 200,
      });

      done();
    });
  });

  it('should log an error when adding routes after adding the default route', (done) => {
    console.log = jest.fn();

    const faas = initialise(mockApiGatewayEvent, jest.fn());
    faas.default(() => Promise.resolve({}));
    faas.get('arbitrary-route', () => Promise.resolve({}));

    setImmediate(() => {
      expect(console.log.mock.calls.length).toBe(1);
      expect(console.log.mock.calls[0][0]).toBe('Routes added after setting the default route.');

      done();
    });
  });

  it('should skip middleware with non-matching routes', (done) => {
    const callback = jest.fn();

    const event = {
      ...mockApiGatewayEvent,
      httpMethod: 'GET',
      path: '/arbitrary/some-route/',
      pathParameters: {
        tail: 'some-route/',
      },
    };
    const faas = initialise(event, callback);
    faas.get('/non-matching-route', () => Promise.resolve({ arbitrary: 'response' }));
    faas.get('/some-route', () => Promise.resolve({ some: 'response' }));

    setImmediate(() => {
      expect(callback.mock.calls.length).toBe(1);
      expect(callback.mock.calls[0][1]).toEqual({
        body: '{"some":"response"}',
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 200,
      });

      done();
    });
  });

  it('should call the API Gateway callback with errors thrown by matching middleware', (done) => {
    const callback = jest.fn();

    const event = {
      ...mockApiGatewayEvent,
      httpMethod: 'GET',
      path: '/arbitrary/some-route/',
      pathParameters: {
        tail: 'some-route/',
      },
    };
    const faas = initialise(event, callback);
    faas.get('/some-route', () => Promise.reject(new Error('Some error')));

    setImmediate(() => {
      expect(callback.mock.calls.length).toBe(1);
      expect(callback.mock.calls[0][1]).toEqual({
        body: 'Some error',
        headers: {
          'Content-Type': 'text/plain',
        },
        statusCode: 400,
      });

      done();
    });
  });

  it('should call the API Gateway callback with the response provided by matching middleware', (done) => {
    const callback = jest.fn();

    const event = {
      ...mockApiGatewayEvent,
      httpMethod: 'GET',
      path: '/arbitrary/some-route/',
      pathParameters: {
        tail: 'some-route/',
      },
    };
    const faas = initialise(event, callback);
    faas.get('/some-route', () => Promise.resolve({ some: 'response' }));

    setImmediate(() => {
      expect(callback.mock.calls.length).toBe(1);
      expect(callback.mock.calls[0][1]).toEqual({
        body: '{"some":"response"}',
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 200,
      });

      done();
    });
  });

  it('should also match the empty route', (done) => {
    const callback = jest.fn();

    const event = {
      ...mockApiGatewayEvent,
      httpMethod: 'GET',
      path: '/arbitrary/',
      pathParameters: {
        tail: '',
      },
    };
    const faas = initialise(event, callback);
    faas.get('/', () => Promise.resolve({ some: 'response' }));

    setImmediate(() => {
      expect(callback.mock.calls.length).toBe(1);
      expect(callback.mock.calls[0][1]).toEqual({
        body: '{"some":"response"}',
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 200,
      });

      done();
    });
  });

  it('should only call the callback with the first matching middleware\'s response', (done) => {
    const callback = jest.fn();

    const event = {
      ...mockApiGatewayEvent,
      httpMethod: 'GET',
      path: '/arbitrary/some-route/',
      pathParameters: {
        tail: 'some-route/',
      },
    };
    const faas = initialise(event, callback);
    faas.get('/(.*)', () => Promise.resolve({ some: 'response' }));
    // This one should never match (and should, in real code, be located before the above one):
    faas.get('/some-route', () => Promise.resolve({ some: 'other response' }));

    setImmediate(() => {
      expect(callback.mock.calls.length).toBe(1);
      expect(callback.mock.calls[0][1]).toEqual({
        body: '{"some":"response"}',
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 200,
      });

      done();
    });
  });

  it('should convert JSON to Javascript objects before passing them to the middleware', (done) => {
    const callback = jest.fn();

    const mockMiddleware = jest.fn().mockReturnValue(Promise.resolve({ arbitrary: 'response' }));

    const event = {
      ...mockApiGatewayEvent,
      body: JSON.stringify({ some: 'body' }),
      httpMethod: 'GET',
      path: '/arbitrary/some-route/',
    };
    const faas = initialise(event, callback);
    faas.get('/(.*)', mockMiddleware);

    setImmediate(() => {
      expect(mockMiddleware.mock.calls.length).toBe(1);
      expect(mockMiddleware.mock.calls[0][0].body).toBeDefined();
      expect(mockMiddleware.mock.calls[0][0].body).toEqual({ some: 'body' });

      done();
    });
  });
});
