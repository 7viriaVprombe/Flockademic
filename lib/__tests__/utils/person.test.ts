import { Person } from '../../interfaces/Person';
import { getOrcid } from '../../utils/person';

describe('getOrcid', () => {
  it('should extract the ORCID from a `sameAs` ORCID link', () => {
    const mockPerson: Partial<Person> = {
      sameAs: 'https://orcid.org/0000-0002-4013-9889',
    };

    expect(getOrcid(mockPerson.sameAs)).toBe('0000-0002-4013-9889');
  });

  it('should extract the ORCID from a `sameAs` ORCID link buried in other links', () => {
    const mockPerson: Partial<Person> = {
      sameAs: [
        { roleName: 'Arbitrary website name', sameAs: 'arbitrary-url' },
        'https://orcid.org/0000-0002-4013-9889',
        { roleName: 'Arbitrary website name', sameAs: 'arbitrary-url' },
      ],
    };

    expect(getOrcid(mockPerson.sameAs)).toBe('0000-0002-4013-9889');
  });

  it('should extract the ORCID even when the ORCID ends with an `X`', () => {
    // See https://support.orcid.org/knowledgebase/articles/116780-structure-of-the-orcid-identifier
    const mockPerson: Partial<Person> = {
      sameAs: 'https://orcid.org/0000-0002-4013-988X',
    };

    expect(getOrcid(mockPerson.sameAs)).toBe('0000-0002-4013-988X');
  });

  it('should return null when a single link is not to an ORCID', () => {
    const mockPerson: Partial<Person> = {
      sameAs: 'arbitrary-url',
    };

    expect(getOrcid(mockPerson.sameAs)).toBeNull();
  });

  it('should return null when none of the links are to an ORCID', () => {
    const mockPerson: Partial<Person> = {
      sameAs: [
        { roleName: 'Arbitrary website name', sameAs: 'arbitrary-url' },
        { roleName: 'Arbitrary website name', sameAs: 'arbitrary-url' },
      ],
    };

    expect(getOrcid(mockPerson.sameAs)).toBeNull();
  });
});
