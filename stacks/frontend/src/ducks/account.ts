import { Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { getAccount, getSession, getToken, verifyOrcid as sendVerifyOrcidRequest } from '../services/account';
import { FluxStandardAction } from './app';

const InitiateOrcidVerificationActionType: 'flockademic/orcid/initiateVerification'
  = 'flockademic/orcid/initiateVerification';
export interface InitiateOrcidVerificationAction extends FluxStandardAction {
  type: 'flockademic/orcid/initiateVerification';
}
const AbortOrcidVerificationActionType: 'flockademic/orcid/abortVerification' = 'flockademic/orcid/abortVerification';
interface AbortOrcidVerificationAction extends FluxStandardAction {
  type: 'flockademic/orcid/abortVerification';
}
const FailOrcidVerificationActionType: 'flockademic/orcid/failVerification' = 'flockademic/orcid/failVerification';
interface FailOrcidVerificationAction extends FluxStandardAction {
  type: 'flockademic/orcid/failVerification';
  payload: { error: Error; };
}
const FailOrcidHydrationActionType: 'flockademic/orcid/failHydration' = 'flockademic/orcid/failHydration';
interface FailOrcidHydrationAction extends FluxStandardAction {
  type: 'flockademic/orcid/failHydration';
}
const VerifyOrcidActionType: 'flockademic/orcid/verify' = 'flockademic/orcid/verify';
interface VerifyOrcidAction extends FluxStandardAction {
  type: 'flockademic/orcid/verify';
  payload: { accountId: string; orcid: string; };
}

export interface AccountState {
  identifier?: string;
  orcid?: string;
  verifying: boolean;
}

export function initiateOrcidVerification(redirectUri?: string, code?: string)
: ThunkAction<
    void,
    AccountState,
    {},
    InitiateOrcidVerificationAction | VerifyOrcidAction | FailOrcidHydrationAction | FailOrcidVerificationAction
  > {
  return async (
    dispatch: Dispatch<
      InitiateOrcidVerificationAction | VerifyOrcidAction | FailOrcidHydrationAction | FailOrcidVerificationAction
    >,
  ) => {
    const initiate: InitiateOrcidVerificationAction = { type: InitiateOrcidVerificationActionType };
    dispatch(initiate);

    try {
      const storedAccount = await getAccount();
      if (storedAccount !== null) {
        const verifyWithStoredOrcid: VerifyOrcidAction = {
          payload: {
            accountId: storedAccount.identifier,
            orcid: storedAccount.orcid,
          },
          type: VerifyOrcidActionType,
        };
        dispatch(verifyWithStoredOrcid);

        return;
      } else if (typeof code === 'undefined' || typeof redirectUri === 'undefined') {
        const failHydration: FailOrcidHydrationAction = { type: FailOrcidHydrationActionType };
        dispatch(failHydration);

        return;
      }
      const session = await getSession();
      const token = await getToken(session.identifier);
      const account = await sendVerifyOrcidRequest(session, token, code, redirectUri);
      const verify: VerifyOrcidAction = {
        payload: {
          accountId: account.identifier,
          orcid: account.orcid,
        },
        type: VerifyOrcidActionType,
      };
      dispatch(verify);
    } catch (e) {
      const fail: FailOrcidVerificationAction = {
        payload: {
          error: new Error('Something went wrong linking your ORCID, please try again.'),
        },
        type: FailOrcidVerificationActionType,
      };
      dispatch(fail);
    }
  };
}

export function abortOrcidVerification(): AbortOrcidVerificationAction {
  return {
    type: 'flockademic/orcid/abortVerification',
  };
}

const initialState: AccountState = { verifying: false };

export function reducer(prevState: AccountState = initialState, action: FluxStandardAction): AccountState {
  if (isInitiateOrcidVerificationAction(action)) {
    return { verifying: true };
  }
  if (isFailOrcidVerificationAction(action)) {
    return { verifying: false };
  }
  if (isFailOrcidHydrationAction(action)) {
    return { verifying: false };
  }
  if (isAbortOrcidVerificationAction(action)) {
    return { verifying: false };
  }
  if (isVerifyOrcidAction(action)) {
    return { orcid: action.payload.orcid, verifying: false };
  }

  return prevState;
}

// These functions will hopefully become redundant if this issue gets resolved:
// https://github.com/Microsoft/TypeScript/issues/10485
export function isInitiateOrcidVerificationAction(action: FluxStandardAction):
action is InitiateOrcidVerificationAction {
  return action.type === InitiateOrcidVerificationActionType;
}
export function isAbortOrcidVerificationAction(action: FluxStandardAction): action is AbortOrcidVerificationAction {
  return action.type === AbortOrcidVerificationActionType;
}
export function isFailOrcidVerificationAction(action: FluxStandardAction): action is FailOrcidVerificationAction {
  return action.type === FailOrcidVerificationActionType;
}
export function isFailOrcidHydrationAction(action: FluxStandardAction): action is FailOrcidHydrationAction {
  return action.type === FailOrcidHydrationActionType;
}
export function isVerifyOrcidAction(action: FluxStandardAction): action is VerifyOrcidAction {
  return action.type === VerifyOrcidActionType;
}
