import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

import { ArticleList } from '../../src/components/articleList/component';
import {
  ProfileOverview,
} from '../../src/components/profileOverview/component';

const mockProps = {
  person: {
    identifier: 'arbitrary_id',
    name: 'Arbitrary name',
  },
  url: 'https://flockademic.com/journal/arbitrary_id',
};

it('should display the profile details when the minimal profile details are loaded', () => {
  const mockPerson = {
    identifier: 'some_identifier',
    name: 'Some name',
  };
  const overview = shallow(<ProfileOverview {...mockProps} person={mockPerson} />);

  expect(toJson(overview)).toMatchSnapshot();
});

it('should display the profile details when the maximum amount of profile details are loaded', () => {
  const mockPerson = {
    affliation: [
      { roleName: 'Some position', affliation: { name: 'Some institution' } },
      { affliation: { name: 'Some institution without a role' } },
      { endDate: 'A former position', roleName: 'Arbitrary position', affliation: { name: 'Arbitrary institution' } },
    ],
    description: 'Some description',
    identifier: 'some_identifier',
    name: 'Some name',
    sameAs: [ { roleName: 'Some website', sameAs: 'some-url' } ],
  };
  const mockArticles = [
    { identifier: 'some_article_id', name: 'Some article name' },
  ];
  const overview = shallow(<ProfileOverview {...mockProps} person={mockPerson} articles={mockArticles} />);

  expect(toJson(overview)).toMatchSnapshot();
});

it('should not list former positions or educations', () => {
  const mockPerson = {
    ...mockProps.person,
    affliation: [
      { endDate: 'Former affliation', roleName: 'Arbitrary role', affliation: { name: 'Arbitrary institution' } },
    ],
  };
  const overview = shallow(<ProfileOverview {...mockProps} person={mockPerson} />);

  expect(overview.find('aside')).not.toExist();
});

it('should also list a website when it is the only one known', () => {
  const mockPerson = {
    ...mockProps.person,
    sameAs: 'https://twitter.com/Flockademic',
  };
  const overview = shallow(<ProfileOverview {...mockProps} person={mockPerson} />);

  expect(overview.find('OutboundLink[to="https://twitter.com/Flockademic"]')).toExist();
});

it('should simply display a website URL when no name has been configured', () => {
  const mockPerson = {
    ...mockProps.person,
    sameAs: [ { sameAs: 'https://twitter.com/Flockademic' } ],
  };
  const overview = shallow(<ProfileOverview {...mockProps} person={mockPerson} />);

  expect(overview.find('OutboundLink[to="https://twitter.com/Flockademic"]').prop('title'))
    .toBe('https://twitter.com/Flockademic');
});

it('should not display the top matter if no description and profile details are known', () => {
  const mockPerson = {
    ...mockProps.person,
    description: undefined,
    sameAs: [],
  };
  const overview = shallow(<ProfileOverview {...mockProps} person={mockPerson} />);

  expect(overview.find('[data-test-id="topMatter"]')).not.toExist();
});

it('should add a link to submit a new article to the article list when the profile is the current user\'s', () => {
  const mockArticles = [
    { identifier: 'some_article_id', name: 'Some article', description: 'Some abstract' },
  ];

  const overview = shallow(<ProfileOverview {...mockProps} profileIsCurrentUser={true} articles={mockArticles} />);

  expect(overview.find(ArticleList)).toExist();
  expect(overview.find(ArticleList).prop('submissionLink')).toBe('/articles/new');
});

it('should display a link to submit a new article when the user has not published any yet', () => {
  const overview = shallow(<ProfileOverview {...mockProps} articles={[]} />);

  expect(overview.find('Link[to="/articles/new"]')) .toExist();
});

it('should display an error message when the person\'s articles could not be loaded', () => {
  const articlelessMockProps = {
    ...mockProps,
    articles: null,
    person: {
      ...mockProps.person,
      name: 'Some name',
    },
  };

  const overview = shallow(<ProfileOverview {...articlelessMockProps} />);

  expect(overview.find('.message.is-danger').last()).toExist();
  expect(overview.find('.message.is-danger').last().text())
    .toMatch('Could not load articles by Some name, please try again.');
});

it('should also list an author\'s articles', () => {
  const mockArticles = [
    { identifier: 'some_article_id', name: 'Some article', description: 'Some abstract' },
  ];

  const overview = shallow(<ProfileOverview {...mockProps} articles={mockArticles} />);

  expect(overview.find(ArticleList)).toExist();
});

// tslint:disable-next-line:max-line-length
it('should display the suggestion to claim a profile when it is unclaimed and the current user is not logged in', () => {
  const overview = shallow(<ProfileOverview {...mockProps} isFlockademicUser={false} currentUser={undefined} />);

  expect(overview.find('.message.is-info')).toExist();
  expect(overview.find('.message.is-info').text())
    .toMatch('This profile has not been claimed yet. Is this you?');
});

it('should display the suggestion to submit an article if this user has not submitted any yet', () => {
  const overview = shallow(
    <ProfileOverview {...mockProps} articles={[]} isFlockademicUser={true} profileIsCurrentUser={true} />,
  );

  expect(overview.find('.message.is-info')).toExist();
  expect(overview.find('.message.is-info').text())
    .toMatch('This is your profile, but it\'s rather empty right now. Give it some body;');
  expect(overview.find('Link[to="/articles/new"]')).toExist();
});

it('should display the suggestion to share one\'s well-rounded profile on Twitter', () => {
  const mockArticles = [
    { identifier: 'arbitrary-id', name: 'Some article' },
  ];

  const overview = shallow(
    <ProfileOverview {...mockProps} articles={mockArticles} isFlockademicUser={true} profileIsCurrentUser={true} />,
  );

  expect(overview.find('.message.is-info')).toExist();
  expect(overview.find('.message.is-info').text())
    .toMatch('This is your profile—looks good!');
  expect(overview.find('OutboundLink[eventLabel="profile-tweet"]')).toExist();
});
